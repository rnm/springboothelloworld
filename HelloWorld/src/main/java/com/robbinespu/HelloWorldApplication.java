package com.robbinespu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;

@SpringBootApplication
//@Configuration @EnableAutoConfiguration @ComponentScan
public class HelloWorldApplication {
	 private static final Logger logger = LoggerFactory.getLogger(HelloWorldApplication.class);

	public static void main(String[] args) {
		//SpringApplication.run(HelloWorldApplication.class, args);
		SpringApplication app = new SpringApplication(HelloWorldApplication.class);
		app.setBannerMode(Banner.Mode.LOG);
		
		//ConfigurableApplicationContext context = SpringApplication.run(HelloWorldApplication.class, args);
        
		app.run(args);
		logger.warn("warning msg");
        logger.error("error msg");
        logger.debug("debug msg");
        logger.info("info msg");
        logger.trace("trace msg");
	}

}
