package com.robbinespu.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.robbinespu.model.Status;

@RestController
public class StatusController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	
	@PostMapping(path = "/receive_endpoint")
    public HashMap<String, String> stationResponse(@RequestBody Status status) {

        /* You can write your DAO logic here.
         * For time being I am printing the customer data just to show the POST call is working.
         */
		LOGGER.debug("ROB: Received {} / {} / {} ",status.getTransferId(),status.getTransferId(),status.getInfo());
		
		HashMap<String, String> map = new HashMap<>();
	    map.put("info", "this is mock server created by Robbi");
	    map.put("status", "SUCCESS");
	    map.put("transfer_id", status.getTransferId().toString());
	    LOGGER.debug("ROB: Replied with {} ",map);
	    return map;
		
        //return "information saved successfully ::." + status.getInfo() + " "+ status.getStatus()+ " "+ status.getTransferId();
    }

}
