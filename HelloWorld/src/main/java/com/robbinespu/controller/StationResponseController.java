package com.robbinespu.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.robbinespu.model.StationResponse;

@RestController
public class StationResponseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	
	@PostMapping(path = "/station_response")
    public HashMap<String, String> stationResponse(@RequestBody StationResponse stationResponse) {

        /* You can write your DAO logic here.
         * For time being I am printing the customer data just to show the POST call is working.
         */
		LOGGER.info("ROB Parsed object: {}", stationResponse.getMessagetype());
		LOGGER.info("ROB Parsed object: {}", stationResponse.getDivertTime());
		LOGGER.info("ROB Parsed object: {}", stationResponse.getTuCode());
		LOGGER.info("ROB Parsed object: {}", stationResponse.getStationId());
		LOGGER.info("ROB Parsed object: {}", stationResponse.getErrorCode());
		//return stationResponse.getMessagetype();
		HashMap<String, String> map = new HashMap<>();
	    map.put("info", "this "+ stationResponse.getMessagetype().toUpperCase()+" mock server created by Robbi");
	    map.put("status", "SUCCESS");
	    //map.put("transfer_id", status.getTransferId().toString());
	    LOGGER.debug("ROB: Replied with {} ",map);
	    return map;
    }

}
