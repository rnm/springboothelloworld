package com.robbinespu.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class StationResponse {
	
	public String getMessagetype() {
		return messagetype;
	}

	public void setMessagetype(String messagetype) {
		this.messagetype = messagetype;
	}

	public String getDivertTime() {
		return divertTime;
	}

	public void setDivertTime(String divertTime) {
		this.divertTime = divertTime;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getTuCode() {
		return tuCode;
	}

	public void setTuCode(String tuCode) {
		this.tuCode = tuCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/*
	 * "{
	 *   nested":
	 *   [],
	 *   "parameters":
	 *   			{
	 *   				"divert_time":"20200911180533",
	 *   				"station_id":"CHK01",
	 *   				"TU_CODE":"TC-00005",
	 *   				"error_code":"ER002"
	 *   			},
	 *   "messagetype":"station_response"
	 *   }'
	 */
	
	String messagetype;
	String divertTime;
	String stationId;
	String tuCode;
	String errorCode;
	
	@JsonProperty("parameters")
	private void parametersDeserializer(Map<String, Object> parameters){
		this.divertTime =  (String) parameters.get("divert_time");
		this.stationId = (String) parameters.get("station_id");
		this.tuCode = (String) parameters.get("TU_CODE");
		this.errorCode = (String) parameters.get("error_code");
	}
	
	public StationResponse() {
		// blank
	}
	
	public StationResponse(String messagetype,Map<String, Object> parameters) {
		this.messagetype = messagetype;
		this.parametersDeserializer(parameters);
	}
	
}
