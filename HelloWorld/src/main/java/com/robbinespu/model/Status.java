package com.robbinespu.model;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Status {
	private static final Logger LOGGER = LoggerFactory.getLogger(Status.class);
	
	BigDecimal transferId;
	String info;
	String status;
	
	public Status() {
		// blank
	}
	
	public Status(BigDecimal transferId,String info,String status) {
		LOGGER.debug("ROB: we got {}",transferId);
		this.info = info;
		this.status = status;
		this.transferId = transferId;
	}

	public BigDecimal getTransferId() {
		return transferId;
	}

	public void setTransferId(BigDecimal transferId) {
		this.transferId = transferId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
